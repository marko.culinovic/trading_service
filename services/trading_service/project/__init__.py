import os
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy


# instantiate the db
db = SQLAlchemy()


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)

    @app.route("/users/ping", methods=["GET"])
    def ping_pong():
        return jsonify({"status": "success", "message": "pong"})

    # register blueprint
    from project.api.inventory import inventory_blueprint

    app.register_blueprint(inventory_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
