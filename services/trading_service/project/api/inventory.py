from flask import Blueprint, request, jsonify

from project import db
from project.api.models import User, Trade, Item, TradeItem


inventory_blueprint = Blueprint("inventory", __name__, url_prefix="/inventory")


@inventory_blueprint.route("/trades", methods=["POST"])
def initiate_trade():
    """ Initiates trade between users """
    # First of the users should send it
    # Request shoud be in JSON and containing both usernames
    # Response contains trade_id inside json and status OK

    data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload."}
    if not data or not data.get("username_1") or not data.get("username_2"):
        return jsonify(response_object), 400
    username_1 = data.get("username_1")
    username_2 = data.get("username_2")

    # validate usernames
    user1_data = db.session.query(User).filter_by(username=username_1).first()
    user2_data = db.session.query(User).filter_by(username=username_2).first()
    if not user1_data or not user2_data:
        response_object["message"] = "Provided usernames do not exist."
        return jsonify(response_object), 400
    try:
        trade = Trade(user1_data.id, user2_data.id)
        db.session.add(trade)
        db.session.commit()
        response_object["status"] = "success"
        response_object["message"] = "Trade created."
        response_object["data"] = {"trade_id": trade.id}
        return jsonify(response_object), 201
    except ValueError:
        db.session.rollback()
        return jsonify(response_object), 400


@inventory_blueprint.route("/trades", methods=["GET"])
def fetch_trades():
    # Request should contain username inside JSON
    # Response returns list of trade_ids for given user and status OK
    data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload."}
    if not data or not data.get("username"):
        return jsonify(response_object), 400
    username = data.get("username")

    # validate usernames
    user_data = db.session.query(User).filter_by(username=username).first()
    if not user_data:
        response_object["message"] = "Provided username do not exist."
        return jsonify(response_object), 400
    try:
        trade_ids = [
            trade.id
            for trade in Trade.query.filter(
                (Trade.user1_id == user_data.id) | (Trade.user2_id == user_data.id)
            ).all()
        ]
        response_object["status"] = "success"
        response_object["message"] = "Trade ids fetched."
        response_object["data"] = {"trade_ids": trade_ids}
        return jsonify(response_object), 200
    except ValueError:
        db.session.rollback()
        return jsonify(response_object), 400


@inventory_blueprint.route("/trades/<int:trade_id>", methods=["POST"])
def modify_trade(trade_id):
    # Request should be in JSON format
    # It should contain following:
    # username
    # action_type: "add_item", "remove_item", "agree"
    # If type is add or remove then it should contain item_id
    data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload."}
    if not data or not data.get("username") or not data.get("action_type"):
        return jsonify(response_object), 400
    username = data.get("username")
    action_type = data.get("action_type")

    # validate usernames
    user_data = db.session.query(User).filter_by(username=username).first()
    if not user_data:
        response_object["message"] = "Provided username does not exist."
        return jsonify(response_object), 400

    trade_data = db.session.query(Trade).filter_by(id=trade_id).first()
    if not trade_data:
        response_object["message"] = "Trade with given id does not exist."
        return jsonify(response_object), 400

    if action_type == "confirm":
        try:
            if trade_data.user1_id == user_data.id:
                trade_data.agreed1 = True
            elif trade_data.user2_id == user_data.id:
                trade_data.agreed2 = True
            if trade_data.agreed1 and trade_data.agreed2:
                # Fetch user1 trade items
                user1_items = (
                    db.session.query(TradeItem)
                    .filter(
                        (TradeItem.user_id == trade_data.user1_id)
                        & (TradeItem.trade_id == trade_data.id)
                    )
                    .all()
                )
                # Fetch user2 trade items
                user2_items = (
                    db.session.query(TradeItem)
                    .filter(
                        (TradeItem.user_id == trade_data.user2_id)
                        & (TradeItem.trade_id == trade_data.id)
                    )
                    .all()
                )
                # Insert into user items for user1
                for item in user2_items:
                    trade_data.first_user.items.append(item.item)
                # Insert into user items for user2
                for item in user1_items:
                    trade_data.second_user.items.append(item.item)

                # Delete items
                for item in user1_items:
                    db.session.delete(item)
                for item in user2_items:
                    db.session.delete(item)
            db.session.commit()
            response_object["status"] = "success"
            response_object["message"] = "Trade confirmed."
            return jsonify(response_object), 201
        except ValueError:
            db.session.rollback()
            return jsonify(response_object), 400

    elif action_type == "add_item":
        try:
            item_id = data.get("item_id")
            if not item_id:
                return jsonify(response_object), 400
            item_data = db.session.query(Item).filter_by(id=item_id).first()
            if not item_data:
                response_object["message"] = "Item with given id does not exist."
                return jsonify(response_object), 400

            user_item = (
                db.session.query(User).filter(User.items.any(id=item_data.id)).first()
            )
            if not user_item:
                response_object["message"] = "User does not have given item."
                return jsonify(response_object), 400
            user_data.items.remove(item_data)
            trade_item = TradeItem(trade_data.id, item_data.id, user_data.id)
            db.session.add(trade_item)
            db.session.commit()
            response_object["status"] = "success"
            response_object["message"] = "Item added."
            return jsonify(response_object), 201
        except ValueError:
            db.session.rollback()
            return jsonify(response_object), 400
    elif action_type == "remove_item":
        try:
            item_id = data.get("item_id")
            if not item_id:
                return jsonify(response_object), 400
            item_data = db.session.query(Item).filter_by(id=item_id).first()
            if not item_data:
                response_object["message"] = "Item with given id does not exist."
                return jsonify(response_object), 400
            trade_item = (
                db.session.query(TradeItem)
                .filter(
                    (TradeItem.user_id == user_data.id)
                    & (TradeItem.trade_id == trade_data.id)
                    & (TradeItem.item_id == item_data.id)
                )
                .first()
            )
            db.session.delete(trade_item)
            user_data.items.append(item_data)
            db.session.commit()
            response_object["status"] = "success"
            response_object["message"] = "Item removed."
            return jsonify(response_object), 201
        except ValueError:
            db.session.rollback()
            response_object["message"] = "Value error."
            return jsonify(response_object), 400

    response_object["message"] = "Invalid action."
    return jsonify(response_object), 400


@inventory_blueprint.route("/trades/<int:trade_id>", methods=["GET"])
def get_trade_items():
    # Request should be in JSON format and contain username
    # Response returns list of trade_items for given user and trade
    pass
