from project import db

user_items = db.Table(
    "user_items",
    db.Column("user_id", db.Integer, db.ForeignKey("users.id")),
    db.Column("item_id", db.Integer, db.ForeignKey("items.id")),
    db.Column("room_id", db.Integer, nullable=True),
    db.Column("avatar_id", db.Integer, nullable=True),
)


class User(db.Model):

    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    items = db.relationship(
        "Item",
        secondary=user_items,
        backref=db.backref("users", lazy="dynamic"),
        lazy="dynamic",
    )
    trade_items = db.relationship("TradeItem", backref="user")

    def __init__(self, username):
        self.username = username


class Item(db.Model):

    __tablename__ = "items"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False, unique=True)
    item_type = db.Column(db.String(20), nullable=False)
    trade_items = db.relationship("TradeItem", backref="item")

    def __init__(self, name, item_type):
        self.name = name
        self.item_type = item_type


class Trade(db.Model):

    __tablename__ = "trades"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user1_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user2_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    first_user = db.relationship("User", foreign_keys=user1_id)
    second_user = db.relationship("User", foreign_keys=user2_id)
    agreed1 = db.Column(db.Boolean)
    agreed2 = db.Column(db.Boolean)
    items = db.relationship("TradeItem", backref="trade")

    def __init__(self, user1_id, user2_id):
        self.user1_id = user1_id
        self.user2_id = user2_id
        self.agreed1 = False
        self.agreed2 = False


class TradeItem(db.Model):

    __tablename__ = "trade_items"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    trade_id = db.Column(db.Integer, db.ForeignKey("trades.id"))
    item_id = db.Column(db.Integer, db.ForeignKey("items.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))

    def __init__(self, trade_id, item_id, user_id):
        self.trade_id = trade_id
        self.item_id = item_id
        self.user_id = user_id
