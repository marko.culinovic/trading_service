import os


class BaseConfig:
    """ Base configuration """

    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = b"\x8b\xfa\xc3\x84\xc1\xf1\xfd\xa0\xa2\xb0\xd0\x1fQq\xe9h"


class DevelopmentConfig(BaseConfig):
    """ Development configuration """

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")


class TestingConfig(BaseConfig):
    """ Testing configuration """

    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_TEST_URL")


class ProductionConfig(BaseConfig):
    """ Production configuration """

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
