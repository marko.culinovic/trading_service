from flask.cli import FlaskGroup
from project import create_app, db
from project.api.models import User, Item

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command()
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def seed_db():
    # Users
    user_1 = User(username="mculinovic")
    user_2 = User(username="ttvrtkovic")
    user_3 = User(username="jimmy")

    # Items
    item1 = Item(name="table", item_type="furniture")
    item2 = Item(name="chair", item_type="furniture")
    item3 = Item(name="red dress", item_type="clothing")
    item4 = Item(name="blue dress", item_type="clothing")
    item5 = Item(name="suit", item_type="clothing")
    item6 = Item(name="shirt", item_type="clothing")

    # User items
    user_1.items.append(item1)
    user_1.items.append(item2)
    user_2.items.append(item3)
    user_2.items.append(item4)
    user_3.items.append(item5)
    user_3.items.append(item6)

    db.session.add(user_1)
    db.session.add(user_2)
    db.session.add(user_3)

    db.session.commit()


if __name__ == "__main__":
    cli()
